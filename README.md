# gdget

![](https://img.shields.io/badge/written%20in-bash-blue)

A gallery downloader for Google Drive shared folders.

Tags: scraper

At the time, there doesn't seem to be any 'download all' link for a shared Google Drive folder full of photos. However the HTML is simple to scrape.

- Accepts URLs in the format `drive.google.com/folderview?id=0B_XXXYYY` 
- Tested on Cygwin
- Requires Curl
- Works as of 2015-01-23, but i confidently expect breaking changes to the HTML in future


## Download

- [⬇️ gdget.sh](dist-archive/gdget.sh) *(813B)*
