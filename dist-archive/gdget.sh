#!/bin/bash
# gdget.sh

url_to_imagelist() {
	curl -s "$1" | sed -En 's/.*,,"([^"]+)".+,,,"(0B_[^"]+?)".*/\2\t\1/p'
}

get_image_url() {
	curl -s 'https://drive.google.com/uc?id='"$1"'&export=download' \
		| grep -Eo 'HREF=".+"' | cut -b7- | rev | cut -b2- | rev
}

download_image() {
	local IMG_ID=$(echo "$1" | cut -d$'\t' -f1)
	local DESTNM=$(echo "$1" | cut -d$'\t' -f2)
	
	curl $(get_image_url "$IMG_ID") > "$DESTNM"
}

download_imagelist() {
	# can't use xargs to a bash function - and the usual workaround is
	#  blocked by shellshock patches
	while read -r line ; do
		download_image "$line"
	done
}

if [[ "$1" == *drive.google.com/folderview\?id=* ]] ; then
	url_to_imagelist "$1" | download_imagelist
	exit 0
else
	echo "Usage: gdget.com https://drive.google.com/folderview?id=0B_XXXXX" >&2
	exit 1
fi
